﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Core.cs.Utilities
{
    public static class ColorUtilities
    {

        public static string RgbToHsv(string rgbString)
        {
            rgbString = rgbString.Trim('#');

            var r = int.Parse(rgbString.Substring(0, 2), NumberStyles.HexNumber);
            var g = int.Parse(rgbString.Substring(2, 2), NumberStyles.HexNumber);
            var b = int.Parse(rgbString.Substring(4, 2), NumberStyles.HexNumber);


            float _R = (r / 255f);
            float _G = (g / 255f);
            float _B = (b / 255f);

            float _Min = Math.Min(Math.Min(_R, _G), _B);
            float _Max = Math.Max(Math.Max(_R, _G), _B);
            float _Delta = _Max - _Min;

            float H = 0;
            float S = 0;
            float L = (float)((_Max + _Min) / 2.0f);

            if (_Delta != 0)
            {
                if (L < 0.5f)
                {
                    S = (float)(_Delta / (_Max + _Min));
                }
                else
                {
                    S = (float)(_Delta / (2.0f - _Max - _Min));
                }


                if (_R == _Max)
                {
                    H = (_G - _B) / _Delta;
                }
                else if (_G == _Max)
                {
                    H = 2f + (_B - _R) / _Delta;
                }
                else if (_B == _Max)
                {
                    H = 4f + (_R - _G) / _Delta;
                }
            }

            var value = H.ToString() + S.ToString() + L.ToString();
            return value;


        }
    }
}
