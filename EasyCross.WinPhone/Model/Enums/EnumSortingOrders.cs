﻿namespace EasyCross.WinPhone.Model.Enums
{
    public enum EnumSortingOrders
    {
        Code,
        Hue,
        Saturation,
        Lightness
    }
}