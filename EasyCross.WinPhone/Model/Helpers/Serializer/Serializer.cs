﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Resources;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace EasyCross.WinPhone.Model
{
    public static class Serializer
    {


        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public static void SerializeObject<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null) { return; }

            try
            {
                var xmlDocument = new XDocument();

                var serializer = new XmlSerializer(serializableObject.GetType());
                using (var stream = new StreamWriter(fileName))
                {
                    serializer.Serialize(stream, serializableObject);
                    xmlDocument.Save(stream);
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }
        }

        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T DeSerializeObject<T>(Stream stream)
        {
            
            T objectOut = default(T);

            try
            {

                var serializer = new XmlSerializer(typeof(T));
                TextReader tr = new StreamReader(stream);
                objectOut = (T) serializer.Deserialize(tr);
            }
            catch (Exception ex)
            {
                //Log exception here
            }

            return objectOut;
        }

    }
}
