﻿using System;
using System.Collections.Generic;

namespace EasyCross.WinPhone.Model.Quantizers.NearestNeighbor
{
    public class NearestNeighborQuantizer
    {
        private readonly List<Tuple<byte, byte, byte>> _rgbValuesToQuantize;
        private readonly List<Tuple<byte, byte, byte>> _palette;


        public NearestNeighborQuantizer()
        {
            


        }


        private double Distance(Tuple<byte, byte, byte> color1, Tuple<byte, byte, byte> color2)
        {
            int a = (color1.Item1 - color2.Item1);
            int b = (color1.Item2 - color2.Item2);
            int c = (color1.Item3 - color2.Item3);

            return Math.Sqrt(a*a + b*b + c*c);
        }

        private Tuple<byte, byte, byte> NearestColor(Tuple<byte, byte, byte> color, List<Tuple<byte, byte, byte>> neighbors)
        {
            var minDistance = Distance(color, neighbors[0]);
            var minNeighbor = neighbors[0];

            foreach (var neighbor in neighbors)
            {
                var distance = Distance(color, neighbor);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    minNeighbor = neighbor;
                }
            }

            return minNeighbor;
        }

        public NearestNeighborQuantizer(List<Tuple<byte, byte, byte>> rgbValuesToQuantize, List<Tuple<byte,byte,byte>> palette)
        {
            _rgbValuesToQuantize = rgbValuesToQuantize;
            _palette = palette;
        }

        public List<Tuple<byte,byte,Byte>> Quantize()
        {
            for (int index = 0; index < _rgbValuesToQuantize.Count; index++)
            {
                var nearestColor = NearestColor(_rgbValuesToQuantize[index], _palette);
                _rgbValuesToQuantize[index] = nearestColor;
            }

            return _rgbValuesToQuantize;
        }


    }
}
