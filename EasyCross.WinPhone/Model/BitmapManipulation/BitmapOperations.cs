﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using EasyCross.WinPhone.Model.Palettes;
using EasyCross.WinPhone.Model.Quantizers.NearestNeighbor;

namespace EasyCross.WinPhone.Model.BitmapManipulation
{
    public static class BitmapOperations
    {
        /// <summary>
        /// Resulting Writeble bitmap has reduced number of colors
        /// </summary>
        /// <param name="wb"></param>
        /// <returns></returns>
        public static WriteableBitmap Quantize(this WriteableBitmap wb, Palette currentPalette)
        {
            var height = wb.PixelHeight;
            var width = wb.PixelWidth;

            var pixelRgbValues = new List<Tuple<byte, byte, byte>>();

            for (int index = 0; index < wb.Pixels.Length; index++)

            {
                var pixel = wb.Pixels[index];

                byte[] colorArray = BitConverter.GetBytes(pixel);
                byte b = colorArray[0];
                byte g = colorArray[1];
                byte r = colorArray[2];
                pixelRgbValues.Add(new Tuple<byte, byte, byte>(r,g,b));
            }

            //get colors from palette
            var colors = currentPalette.Colors;

            var palette = new List<Tuple<byte, byte, byte>>();
            palette.AddRange(colors.Select(codedColor => ColorUtilities.ParseRgbStringByte(codedColor.ColorString)));


            var quantizer = new NearestNeighborQuantizer(pixelRgbValues, palette);
            
            var result = quantizer.Quantize();
            for (int index = 0; index < result.Count; index++)
            {
                var tuple = result[index];
                Int32 color = BitConverter.ToInt32(new byte[] {tuple.Item3, tuple.Item2, tuple.Item1, (byte) 0xff}, 0);

                wb.Pixels[index] = color;
            }

            return wb;
        }


        public static WriteableBitmap ScaleBack(this WriteableBitmap wb, int w, int h)
        {
            int multiplier;

            multiplier = 20;
            if (w >= 100) multiplier = 10;
            if (w >= 200) multiplier = 5;
            if (w >= 300) multiplier = 2;
            if (w >= 450) multiplier = 1;
            return wb.Resize(w*multiplier, h*multiplier, WriteableBitmapExtensions.Interpolation.NearestNeighbor);
        }

        public static BitmapImage GetImage(this WriteableBitmap wb)
        {
            var image = new BitmapImage();

            using (var stream = new MemoryStream())
            {
                wb.SaveJpeg(stream, wb.PixelWidth,wb.PixelHeight,0,100);
                image.SetSource(stream);
            }

            return image;
        }





    }
}
