﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Resources;

namespace EasyCross.WinPhone.Model.Palettes
{
    public class ColorCollection
    {
        public ColorCollection()
        {
            CodedColors = new List<CodedColor>();
        }

        public List<CodedColor> CodedColors { get; set; }
    }

    public static class ColorCollectionInstance
    {
        public static ColorCollection ColorCollection { get; set; }

        static ColorCollectionInstance()
        {
            StreamResourceInfo xml = Application.GetResourceStream(new Uri("/EasyCross.WinPhone;component/Resources/palette.xml",UriKind.Relative));
            ColorCollection = Serializer.DeSerializeObject<ColorCollection>(xml.Stream);
        }

        public static void Init()
        {
        }

    
    }
}
