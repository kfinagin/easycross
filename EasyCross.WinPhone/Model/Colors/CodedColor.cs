﻿namespace EasyCross.WinPhone.Model.Palettes
{
    [Serializable]
    public class CodedColor
    {
        public string CodeDMC { get; set; }
        public string CodeGamma { get; set; }
        public string CodeAnchor { get; set; }
        public string CodeMadeira { get; set; }
        public string ColorString { get; set; }
    }
}