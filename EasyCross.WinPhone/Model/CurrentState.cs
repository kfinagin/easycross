﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyCross.WinPhone.Model.Enums;
using EasyCross.WinPhone.Model.Palettes;
using EasyCross.WinPhone.ViewModels;

namespace EasyCross.WinPhone.Model
{
    public static class CurrentState
    {
        //current scheme
        static CurrentState()
        {
            CurrentPalette = new ObservableCollection<DisplayedColor>();
        }

        public static Scheme CurrentScheme { get; set; }
       

        public static ObservableCollection<DisplayedColor> CurrentPalette { get; set; } 


        public static bool? IsEditingScheme { get; set; }


        #region Manufacturers and Sorting Orders


        public static DescriptionObject<EnumManufacturers> CurrentManufacturer { get; set; }
        public static DescriptionObject<EnumSortingOrders> CurrentSortingOrder { get; set; }

        public static ObservableCollection<DescriptionObject<EnumManufacturers>> Manufacturers = new ObservableCollection<DescriptionObject<EnumManufacturers>>()
            {
                new DescriptionObject<EnumManufacturers>(){Value = EnumManufacturers.Anchor, Description = "Anchor"},
                new DescriptionObject<EnumManufacturers>(){Value = EnumManufacturers.Madeira, Description = "Madeira"},
                new DescriptionObject<EnumManufacturers>(){Value = EnumManufacturers.DMC, Description = "DMC"},
                new DescriptionObject<EnumManufacturers>(){Value = EnumManufacturers.Gamma, Description = "Gamma"}
            };

        public static ObservableCollection<DescriptionObject<EnumSortingOrders>> SortingOrders = new ObservableCollection<DescriptionObject<EnumSortingOrders>>()
            {
                new DescriptionObject<EnumSortingOrders>(){Value = EnumSortingOrders.Code, Description = "Sort: Code"},
                new DescriptionObject<EnumSortingOrders>(){Value = EnumSortingOrders.Hue, Description = "Sort: Hue"},
                new DescriptionObject<EnumSortingOrders>(){Value = EnumSortingOrders.Saturation, Description = "Sort: Saturation"},
                new DescriptionObject<EnumSortingOrders>(){Value = EnumSortingOrders.Lightness, Description = "Sort: Lightness"},
            };


        #endregion Manufacturers and Sorting Orders

    }
}
