﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using EasyCross.WinPhone.Model.Palettes;

namespace EasyCross.WinPhone.Model.Data
{
    public class Context : DataContext
    {
        public Table<Scheme> Schemes;
        public Table<Palette> Palettes;

        #region Thread-safe singleton implementation

        private Context(string connectionString = DataSettings.ConnectionString)
            : base(connectionString)
        {

        }

        public static Context Instance
        {
            get { return Nested.Instance; }
        }

        private class Nested
        {
            static Nested()
            {
            }
            internal static readonly Context Instance = new Context();
        }

        #endregion Thread-safe singleton implementation
    }
}
