﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using EasyCross.WinPhone.Model.BitmapManipulation;
using EasyCross.WinPhone.Model.Palettes;
using Microsoft.Phone.Tasks;
using Microsoft.Xna.Framework.Media;

namespace EasyCross.WinPhone.Model
{
    [Table]
    public class Scheme : INotifyPropertyChanging, INotifyPropertyChanged
    {
        #region Database Fields

        [Column(IsVersion = true)]
        private Binary _version;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false,
            AutoSync = AutoSync.OnInsert)]
        public long Id { get; set; }

        [Column]
        public string Name
        {
            get { return _name; }
            set
            {
                NotifyPropertyChanging("Name");
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }

        [Column]
        public DateTime Edited
        {
            get { return _edited; }
            set
            {
                NotifyPropertyChanging("Edited");
                _edited = value;
                NotifyPropertyChanged("Edited");
            }
        }

        [Column]
        public string ImageFileString
        {
            get { return _imageFileString; }
            set
            {
                QuantizedBitmap = null;

                NotifyPropertyChanging("ImageFileString");
                _imageFileString = value;
                NotifyPropertyChanged("ImageFileString");
            }
        }

        [Column]
        public int Width
        {
            get
            {
                return _width;
            }
            set
            {

                if (_width == value) return;
                _width = value;
                QuantizedBitmap = null;
                NotifyPropertyChanged("Width");
            }
        }

        [Column]
        public int Height
        {
            get { 
                return _height;
            }
            set
            {
                if (_height == value) return;
                _height = value;
                QuantizedBitmap = null;
                NotifyPropertyChanged("Height");
            }
        }

        [Column]
        public bool PreserveRatio { get; set; }

        [Column] internal int _paletteId;

        //Entity reference to identify the "Palette" Storage
        private EntityRef<Palette> _palette;
        private long _id;

        [Association(Storage = "_palette", ThisKey = "_paletteId", OtherKey = "Id", IsForeignKey = true)]
        public Palette Palette 
        {
            get
            {
                return _palette.Entity;
            }
            set
            {
                NotifyPropertyChanging("Palette");

                QuantizedBitmap = null;
                _palette.Entity = value;

                if (value != null)
                {
                    _paletteId = value.Id;
                }
                NotifyPropertyChanged("Palette");
            } 
        } 

        [Column]
        public byte[] Thumbnail { get; set; }


        #endregion Database Fields

        #region Computed Fields




        #endregion Computed Fields

        private BitmapImage _thumb;
        private BitmapImage _image;


        private int _width;
        private int _height;

        public double Ratio
        {
            get
            {

                if (string.IsNullOrEmpty(ImageFileString)) return 1;

                if (_image == null)
                {
                    return 1;
                }
                return (double)_image.PixelWidth/_image.PixelHeight;
            }
        }

        public BitmapImage Thumb
        {
            get
            {
                using (var ml = new MediaLibrary())
                {
                    using (PictureCollection col = ml.Pictures)
                    {
                        var picture = col.FirstOrDefault(i => i.Name == ImageFileString);
                        if (picture == null) return null;
                        var stream = picture.GetThumbnail();
                        _thumb = new BitmapImage();
                        _thumb.SetSource(stream);

                        return _thumb;
                    }
                }
            }
        }

        public BitmapImage InitialImage
        {
            get
            {

                using (var ml = new MediaLibrary())
                {
                    using (PictureCollection col = ml.Pictures)
                    {

                        if (ImageFileString == null) return null;

                        var picture = col.FirstOrDefault(i => i.Name == ImageFileString);
                        if (picture == null) return null;

                        var stream = picture.GetImage();
                        _image = new BitmapImage();
                        
                        _image.SetSource(stream);

                        var wb = new WriteableBitmap(_image);

                        var w = _image.PixelWidth;
                        var h = _image.PixelHeight;
                        int w1;
                        int h1;

                        if (w > h)
                        {
                            w1 = 600;
                            h1 = w1*h/w;
                        }
                        else
                        {
                            h1 = 600;
                            w1 = h1*w/h;
                        }

                        wb = wb.Resize(w1, h1, WriteableBitmapExtensions.Interpolation.Bilinear);

                        return wb.GetImage();
                    }
                }

            }
        }

        private BitmapImage _quantizedImage;
        private WriteableBitmap _quantizedBitmap;
        private List<string> _pixelCodes;
        private string _name;
        private DateTime _edited;
        private string _imageFileString;

        public WriteableBitmap QuantizedBitmap
        {
            get
            {
                if (_image == null) return null;

                //if not yet quantized - quantize
                var wb = new WriteableBitmap(_image);
                wb = wb.Resize(Width, Height, WriteableBitmapExtensions.Interpolation.Bilinear);

                if (Palette == null) return null;
                wb = wb.Quantize(Palette);

                _quantizedBitmap = wb;
                return _quantizedBitmap;
            }
            set
            {
                _quantizedBitmap = value;
            }
        }

        public BitmapImage QuantizedImage
        {
            get
            {
                var wb = QuantizedBitmap;
                if (wb == null) return null;

                wb = wb.ScaleBack(Width, Height);
                return wb.GetImage();


            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify that a property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify that a property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion

        #region Validation

        public const int MaxSize = 500;

        public List<string> ValidationErrors()
        {
            var errors = new List<string>();

            if(String.IsNullOrEmpty(Name)) errors.Add("Name should not be empty");
            if(Width<=0 || Height <= 0) errors.Add("Width, height should be > 0");
            if(Width >= MaxSize || Height >= MaxSize) errors.Add("Width, height should be <= 500");
            if(Palette == null) errors.Add("Palette not specified");

            return errors;

        }

        #endregion Validation
    }


}
