﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;

namespace EasyCross.WinPhone.Model.Palettes
{
    [Table]
    public class Palette
    {
        #region Database Fields

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int Id { get; set; }

        [Column]
        public string Name { get; set; }

        [Column]
        public DateTime Created { get; set; }

        [Column]
        public string ColorCodes { get; set; }

        #endregion Database Fields

        #region Computed Fields


        private List<CodedColor> _colors; 
        public List<CodedColor> Colors
        {
            get
            {
                if(_colors == null) LoadColors();
                return _colors;
            }
        }

        private void LoadColors()
        {
            var splitColors = ColorCodes.Split(',');
            _colors = ColorCollectionInstance.ColorCollection.CodedColors.Where(i => splitColors.Contains(i.ColorString)).ToList();
        }

        

        #endregion Computed Fields

        public int? GetCode(string color)
        {
            for (int i = 0; i < Colors.Count; i++)
            {
                if (String.Equals(Colors[i].ColorString, color, StringComparison.CurrentCultureIgnoreCase)) return i;
            }
            return null;
        }
    }
}
