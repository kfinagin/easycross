﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCross.WinPhone.Model.Data
{
    public static class DatabaseManager
    {

        public static void CreateDatabase()
        {

            if (Context.Instance.DatabaseExists() == false)
            {
                Context.Instance.CreateDatabase();

            }

        }

        public static void DeleteDatabase()
        {

            if (Context.Instance.DatabaseExists())
            {
                Context.Instance.DeleteDatabase();
            }

        }

    }
}
