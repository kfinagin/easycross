﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;
using EasyCross.WinPhone.Model;
using EasyCross.WinPhone.Model.Data;
using EasyCross.WinPhone.Model.Palettes;
using EasyCross.WinPhone.ViewModels;

namespace EasyCross.WinPhone
{
    public class Bootstrapper : PhoneBootstrapper
    {
        public  PhoneContainer container { get; set; }

        protected override void Configure()
        {
            container = new PhoneContainer();

            container.RegisterPhoneServices(RootFrame);

            container.PerRequest<MainPageViewModel>();

            //scheme pivot
            container.PerRequest<PivotViewModel>();
            container.PerRequest<ImageViewModel>();
            container.PerRequest<SchemeViewModel>();

            //palette pivot
            container.PerRequest<PalettePivotViewModel>();
            container.PerRequest<PaletteViewModel>();
            container.PerRequest<PaletteListViewModel>();

            ColorCollectionInstance.Init();

            AddCustomConventions();

            DatabaseManager.CreateDatabase();
            
        }

        

        static void AddCustomConventions()
        {

            //bindable app bar actions
            ConventionManager.AddElementConvention<BindableAppBarButton>(Control.IsEnabledProperty, "DataContext",
                "Click");
            ConventionManager.AddElementConvention<BindableAppBarMenuItem>(Control.IsEnabledProperty, "DataContext",
                "Click");

        }

        protected override object GetInstance(Type service, string key)
        {
            return container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }
    } 
}
