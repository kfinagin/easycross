﻿using System;
using System.Globalization;

namespace EasyCross.WinPhone.Model
{
    public static class ColorUtilities
    {
        public static Tuple<int, int, int> ParseRgbString(string rgbString)
        {

            rgbString = rgbString.Trim('#');

            var r = int.Parse(rgbString.Substring(0, 2), NumberStyles.HexNumber);
            var g = int.Parse(rgbString.Substring(2, 2), NumberStyles.HexNumber);
            var b = int.Parse(rgbString.Substring(4, 2), NumberStyles.HexNumber);

            return new Tuple<int, int, int>(r,g,b);
        }

        public static Tuple<byte, byte, byte> ParseRgbStringByte(string rgbString)
        {

            rgbString = rgbString.Trim('#');

            var r = byte.Parse(rgbString.Substring(0, 2), NumberStyles.HexNumber);
            var g = byte.Parse(rgbString.Substring(2, 2), NumberStyles.HexNumber);
            var b = byte.Parse(rgbString.Substring(4, 2), NumberStyles.HexNumber);

            return new Tuple<byte, byte, byte>(r, g, b);
        }


        public static Tuple<float, float, float> RgbToHsv(Tuple<int,int,int> rgb)
        {
            float _R = rgb.Item1 / 255f;
            float _G = rgb.Item2 / 255f;
            float _B = rgb.Item3 / 255f;


            float _Min = Math.Min(Math.Min(_R, _G), _B);
            float _Max = Math.Max(Math.Max(_R, _G), _B);
            float _Delta = _Max - _Min;

            float H = 0;
            float S = 0;
            float L = (float)((_Max + _Min) / 2.0f);

            if (_Delta != 0)
            {
                if (L < 0.5f)
                {
                    S = (float)(_Delta / (_Max + _Min));
                }
                else
                {
                    S = (float)(_Delta / (2.0f - _Max - _Min));
                }


                if (_R == _Max)
                {
                    H = (_G - _B) / _Delta;
                }
                else if (_G == _Max)
                {
                    H = 2f + (_B - _R) / _Delta;
                }
                else if (_B == _Max)
                {
                    H = 4f + (_R - _G) / _Delta;
                }
            }

            return new Tuple<float, float, float>(H,S,L);
        }


        public static string RgbToH(string rgbString)
        {
            return RgbToHsv(ParseRgbString(rgbString)).Item1.ToString(CultureInfo.InvariantCulture);
        }

        public static string RgbToS(string rgbString)
        {
            var hsv = RgbToHsv(ParseRgbString(rgbString));
            return hsv.Item2.ToString(CultureInfo.InvariantCulture);
        }

        public static string RgbToL(string rgbString)
        {
            var hsv = RgbToHsv(ParseRgbString(rgbString));
            return hsv.Item3.ToString(CultureInfo.InvariantCulture);
        }


    }
}
