﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using Microsoft.Xna.Framework;

namespace EasyCross.WinPhone.Views.Converters
{
    public class ForegroundColorConverter : IValueConverter
    {
        private SolidColorBrush blackBrush = new SolidColorBrush(Colors.Black);
        private SolidColorBrush whiteBrush = new SolidColorBrush(Colors.White);


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var colorString = value as string;
            if (colorString == null) return null;

            colorString = colorString.Trim('#');
            var r = Byte.Parse(colorString.Substring(0, 2), NumberStyles.HexNumber);
            var g = Byte.Parse(colorString.Substring(2, 2), NumberStyles.HexNumber);
            var b = Byte.Parse(colorString.Substring(4, 2), NumberStyles.HexNumber);

            if (r <= 100 && g <= 100 && b <= 100) return whiteBrush;
            return blackBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
