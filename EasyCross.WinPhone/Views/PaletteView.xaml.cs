﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace EasyCross.WinPhone.Views
{
    public partial class PaletteView : PhoneApplicationPage
    {
        public PaletteView()
        {
            InitializeComponent();
        }

        private void Clean_OnClick(object sender, EventArgs e)
        {
            //очистить выбранные элементы
            this.ColorListBox.SelectedItems.Clear();

        }



        private void DeleteSelectedFromSelected_OnClick(object sender, EventArgs e)
        {
            //также удалить из выбранных
            var selected = this.SelectedColorListBox.SelectedItems;
            foreach (var item in selected)
            {
                this.ColorListBox.SelectedItems.Remove(item);                
            }

        }
    }
}