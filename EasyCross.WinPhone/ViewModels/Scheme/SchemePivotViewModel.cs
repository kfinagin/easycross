﻿using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;

namespace EasyCross.WinPhone.ViewModels
{
    public class SchemePivotViewModel: Conductor<IScreen>.Collection.OneActive
    {
        private readonly ImageViewModel _imageItem;
        private readonly SettingsViewModel _settingsItem;
        private readonly SchemeViewModel _schemeItem;

        public SchemePivotViewModel(ImageViewModel imageItem, SettingsViewModel settingsItem, SchemeViewModel schemeItem)
        {
            this._imageItem = imageItem;
            this._settingsItem = settingsItem;
            this._schemeItem = schemeItem;

            this.Activated += (sender, args) => AppBarConductor.Mixin(this);
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            Items.Add(_imageItem);
            Items.Add(_settingsItem);
            Items.Add(_schemeItem);

            ActivateItem(_imageItem);
        }
    }
}
