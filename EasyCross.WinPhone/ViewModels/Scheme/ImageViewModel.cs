﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Imaging;
using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;
using EasyCross.WinPhone.Model;
using Microsoft.Phone.Tasks;
using TaskResult = Microsoft.Phone.Tasks.TaskResult;

namespace EasyCross.WinPhone.ViewModels
{
    public class ImageViewModel: Screen
    {
        private Scheme _currentScheme;
        private string _schemeName;
        private string _schemeDescription;

        public ImageViewModel()
        {
            DisplayName = "Image";
        }

        #region Properties

        public string SchemeName
        {
            get { return _schemeName; }
            set
            {
                _schemeName = value; 
                NotifyOfPropertyChange(() => SchemeName);
            }
        }

        public string SchemeDescription
        {
            get { return _schemeDescription; }
            set
            {
                _schemeDescription = value;
                NotifyOfPropertyChange(() => SchemeDescription);
            }
        }

        //image to store and convert to storage

        #endregion Properties



        public Scheme SelectedScheme
        {
            get { return SchemeStorage.Instance.CurrentScheme; }
            set
            {
                SchemeStorage.Instance.CurrentScheme = value;
                NotifyOfPropertyChange(() => SelectedScheme);
            }
        }

        public void SaveScheme()
        {

            var newScheme = new Scheme() {Name = SchemeName, Description = SchemeDescription};
            SchemeStorage.Instance.AddScheme(newScheme);
            SchemeStorage.Instance.CurrentScheme = newScheme;


            SchemeStorage.Instance.Save();
            //TODO popup
            
 

        }


        public void LoadImage()
        {
            var photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += (sender, result) => GetImage(result);

            photoChooserTask.Show();

            //show image and save it as a starting point for processing
            
        }

        public void TakePhoto()
        {
            var captureTask = new CameraCaptureTask();
            captureTask.Completed += (sender, result) => GetImage(result);


            captureTask.Show();

            //show image and save it as a starting point for processing
        }




        private void GetImage(PhotoResult result)
        {
            if (result.TaskResult == TaskResult.OK)
            {

                var image = new BitmapImage(new Uri(result.OriginalFileName));

            }


        }
    }
}
