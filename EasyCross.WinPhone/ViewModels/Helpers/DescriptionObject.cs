﻿using System.Collections.Generic;

namespace EasyCross.WinPhone.ViewModels
{
    public class DescriptionObject<T>
    {

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DescriptionObject<T>)obj);
        }

        protected bool Equals(DescriptionObject<T> other)
        {
            return EqualityComparer<T>.Default.Equals(Value, other.Value) && string.Equals(Description, other.Description);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<T>.Default.GetHashCode(Value)*397) ^ (Description != null ? Description.GetHashCode() : 0);
            }
        }


        public T Value { get; set; }
        public string Description { get; set; }




    }
}