﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyCross.WinPhone.Model;
using EasyCross.WinPhone.Model.Enums;
using EasyCross.WinPhone.Model.Palettes;

namespace EasyCross.WinPhone.ViewModels.Helpers
{
    public static class SelectFuncs
    {
        /// <summary>
        /// Selecting manufacturer
        /// </summary>
        /// <param name="manufacturer"></param>
        /// <returns></returns>
        public static Func<CodedColor, DisplayedColor> SelectManufacturer(EnumManufacturers manufacturer)
        {
            switch (manufacturer)
            {
                case EnumManufacturers.Anchor:
                    return i => new DisplayedColor() { CodeString = i.CodeAnchor, ColorString = i.ColorString };
                case EnumManufacturers.DMC:
                    return i => new DisplayedColor() { CodeString = i.CodeDMC, ColorString = i.ColorString };
                case EnumManufacturers.Madeira:
                    return i => new DisplayedColor() { CodeString = i.CodeMadeira, ColorString = i.ColorString };
                case EnumManufacturers.Gamma:
                    return i => new DisplayedColor() { CodeString = i.CodeGamma, ColorString = i.ColorString };
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Selecting sorting order
        /// </summary>
        /// <param name="order"></param>
        /// <param name="maxLen"></param>
        /// <returns></returns>
        public static Func<DisplayedColor, string> SelectSort(EnumSortingOrders order, int maxLen)
        {
            
            switch (order)
            {

                case EnumSortingOrders.Code:
                    return i => i.CodeString.Trim('*').PadLeft(maxLen,'0');
                case EnumSortingOrders.Hue:
                    return i => ColorUtilities.RgbToH(i.ColorString);
                case EnumSortingOrders.Saturation:
                    return i => ColorUtilities.RgbToS(i.ColorString);
                case EnumSortingOrders.Lightness:
                    return i => ColorUtilities.RgbToL(i.ColorString);

                default:
                    throw new ArgumentOutOfRangeException();
            }

        }
    }
}
