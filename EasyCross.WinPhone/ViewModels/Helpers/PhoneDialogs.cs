﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Coding4Fun.Toolkit.Controls;
using Microsoft.Phone.Controls;

namespace EasyCross.WinPhone.ViewModels.Helpers
{
    public static class PhoneDialogs
    {
        public static void DeleteCancel(Action funcOnDelete)
        {
            var box = new CustomMessageBox()
            {
                LeftButtonContent = "Delete",
                RightButtonContent = "Cancel",

            };

            box.Dismissed += (sender, args) =>
            {
                if (args.Result == CustomMessageBoxResult.LeftButton)
                {
                    funcOnDelete();
                }
                else if (args.Result == CustomMessageBoxResult.RightButton)
                {
                    ;//action when cancel pressed
                }
                else
                {
                    ;//action when back key pressed, optional
                }
            };

            box.Show();
        }

        public static void ApproveCancel(string defaultName, Action<string> funcOnApprove)
        {
            var box = new CustomMessageBox()
            {
                LeftButtonContent = "Save",
                RightButtonContent = "Cancel",
                Content = new TextBox()
                {
                    Text = defaultName
                }

            };

            box.Dismissed += (sender, args) =>
            {
                if (args.Result == CustomMessageBoxResult.LeftButton)
                {
                    var textBox = box.Content as TextBox;
                    if (textBox == null) return;
                    var name = textBox.Text;

                    if (String.IsNullOrEmpty(name)) name = defaultName;

                    funcOnApprove(name);

                }
                else if (args.Result == CustomMessageBoxResult.RightButton)
                {
                    ;//action when cancel pressed
                }
                else
                {
                    ;//action when back key pressed, optional
                }
            };

            box.Show();
        }

        public static void ShowMessage(string message)
        {
            var box = new CustomMessageBox()
            {
                Content = new TextBlock()
                {
                    TextWrapping = TextWrapping.Wrap,
                    Margin = new Thickness(10),
                    Text = message
                },
                RightButtonContent = "OK"
            };
            box.Show();
        }

        public static void ShowToast(string message)
        {
            var toast = new ToastPrompt
            {
                Title = "EasyCross",
                Message = message,
                TextOrientation = Orientation.Horizontal,
                MillisecondsUntilHidden = 1000
            };

            toast.Show();
            
        }


        public static void SetXY(Action<int,int> funcOnApprove)
        {
            var tb1 = new TextBlock() { Margin = new Thickness(5), Text = "X: ", VerticalAlignment = VerticalAlignment.Center};
            var tb2 = new TextBlock() { Margin = new Thickness(5), Text = "Y: ", VerticalAlignment = VerticalAlignment.Center };
            var x = new TextBox() {Width = 120, Margin = new Thickness(5), Text = "0", 
                InputScope = new InputScope(){Names = {new InputScopeName() {NameValue = InputScopeNameValue.Number}}}};
            var y = new TextBox() {Width = 120, Margin = new Thickness(5), Text = "0", 
                InputScope = new InputScope(){Names = {new InputScopeName() {NameValue = InputScopeNameValue.Number}}}};


            var sp = new StackPanel() {Orientation = Orientation.Horizontal};
            sp.Children.Add(tb1);
            sp.Children.Add(x);
            sp.Children.Add(tb2);
            sp.Children.Add(y);


            var box = new CustomMessageBox()
            {
                Content = sp,
                
                RightButtonContent = "OK"
            };

            box.Dismissed += (sender, args) =>
            {
                if (args.Result == CustomMessageBoxResult.RightButton)
                {

                        var xcoord = int.Parse(x.Text);
                        var ycoord = int.Parse(y.Text);

                        funcOnApprove(xcoord, ycoord);


                }
                else if (args.Result == CustomMessageBoxResult.LeftButton)
                {
                    ;//action when cancel pressed
                }
                else
                {
                    ;//action when back key pressed, optional
                }
            };

            box.Show();
            
        }
    }
}
