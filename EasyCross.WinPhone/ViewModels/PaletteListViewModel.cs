﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Caliburn.Micro;
using EasyCross.WinPhone.Model;
using EasyCross.WinPhone.Model.Data;
using EasyCross.WinPhone.Model.Palettes;
using EasyCross.WinPhone.ViewModels.Helpers;

namespace EasyCross.WinPhone.ViewModels
{
    public class PaletteListViewModel : Screen
    {
        private readonly INavigationService _navigationService;
        private Palette _selectedPalette;

        public PaletteListViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            DisplayName = "List of palettes";
            Palettes = new ObservableCollection<Palette>();
        }

        protected override void OnActivate()
        {

            LoadPalettes();

        }


        private void LoadPalettes()
        {
            var palettes = Context.Instance.Palettes;
            Palettes.Clear();
            foreach (var palette in palettes)
            {
                Palettes.Add(palette);
            }

            SelectedPalette = Palettes.Count>0 ? Palettes[0] : null;
        }

        public ObservableCollection<Palette> Palettes { get; set; }

        

        public Palette SelectedPalette
        {
            get { return _selectedPalette; }
            set
            {
                if (Equals(value, _selectedPalette)) return;
                _selectedPalette = value;
                NotifyOfPropertyChange(() => SelectedPalette);
            }
        }

        #region Actions

        public void GotoMain()
        {
            _navigationService.UriFor<MainPageViewModel>().Navigate();
        }

        public void Apply()
        {
            if (CurrentState.CurrentScheme == null) return;
            else
            {
                CurrentState.CurrentScheme.Palette = SelectedPalette;
                _navigationService.UriFor<PivotViewModel>().Navigate();   
                
            }
        }

        public void Delete()
        {
            //if current scheme palette == selected palette, then null

            //ask
            PhoneDialogs.DeleteCancel(() =>
            {
                try
                {
                    if (CurrentState.CurrentScheme != null && CurrentState.CurrentScheme.Palette == SelectedPalette) CurrentState.CurrentScheme.Palette = null;
                    Context.Instance.Palettes.DeleteOnSubmit(SelectedPalette);
                    Context.Instance.SubmitChanges();
                    PhoneDialogs.ShowToast("Palette deleted");
                }
                catch (Exception)
                {
                    PhoneDialogs.ShowMessage("Can't delete palette, maybe it is still in use in some scheme");
                }


                LoadPalettes();

            });




        }

        public void GetHelp()
        {
            PhoneDialogs.ShowMessage("\u2022 Press Apply to apply selected palette to current scheme if any." +
                                     "\n\u2022 Press Delete to delete scheme from the list." +
                                     "\n\u2022 Swipe to New palette to create new palette for the scheme.");
        }

        public void ChangeSelection(SelectionChangedEventArgs eventArgs)
        {
            if (eventArgs.AddedItems.Count != 1) return;
            var item = eventArgs.AddedItems[0];
            SelectedPalette = item as Palette;
        }

        #endregion Actions



    }
}






