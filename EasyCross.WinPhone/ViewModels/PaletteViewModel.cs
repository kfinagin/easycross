﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using Caliburn.Micro;
using Clarity.Phone.Extensions;
using EasyCross.WinPhone.Model;
using EasyCross.WinPhone.Model.Data;
using EasyCross.WinPhone.Model.Enums;
using EasyCross.WinPhone.Model.Palettes;
using EasyCross.WinPhone.ViewModels.Helpers;
using Microsoft.Phone.Controls;
using Microsoft.Win32.SafeHandles;


namespace EasyCross.WinPhone.ViewModels
{
    public class PaletteViewModel : Screen
    {
        private readonly INavigationService _navigationService;
        private readonly IWindowManager _windowManager;
        private DescriptionObject<EnumManufacturers> _selectedManufacturer;
        private DescriptionObject<EnumSortingOrders> _selectedSortingOrder;

        private ObservableCollection<DisplayedColor> _codedColors;
        private bool _isBusy;
        private string _busyMessage;
        private bool _deleteSelectedVisible;
        private bool _clearSelectedVisible;

        public PaletteViewModel(INavigationService navigationService, IWindowManager windowManager)
        {
            _navigationService = navigationService;
            _windowManager = windowManager;
            DisplayName = "New palette";

            CodedColors = new ObservableCollection<DisplayedColor>();
            SelectedColors = new BindableCollection<DisplayedColor>();
            SelectedFromSelectedColors = new ObservableCollection<DisplayedColor>();


            SelectedColors.CollectionChanged += SelectedColors_CollectionChanged;
            SelectedFromSelectedColors.CollectionChanged += SelectedFromSelectedColors_CollectionChanged;

            Manufacturers = CurrentState.Manufacturers;
            SortingOrders = CurrentState.SortingOrders;
        }

        #region CollectionChanged Events

        void SelectedColors_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (SelectedColors.Count <= 0)
            {
                ClearSelectedVisible = false;
            }
            else
            {
                ClearSelectedVisible = true;
            }
        }

        void SelectedFromSelectedColors_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (SelectedFromSelectedColors.Count <= 0)
            {
                DeleteSelectedVisible = false;
            }
            else
            {
                DeleteSelectedVisible = true;
            }
        }

        #endregion CollectionChanged Events

        #region ViewLoadedUnloaded Events

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);


            _selectedManufacturer = CurrentState.CurrentManufacturer ?? Manufacturers[0];
            _selectedSortingOrder = CurrentState.CurrentSortingOrder ?? SortingOrders[0];

            NotifyOfPropertyChange(() => SelectedManufacturer);
            NotifyOfPropertyChange(() => SelectedSortingOrder);

            UpdateCodedColors();
           
        }


        protected override void OnDeactivate(bool close)
        {

            //remember currently displayed colors
            CurrentState.CurrentManufacturer = SelectedManufacturer;
            CurrentState.CurrentSortingOrder = SelectedSortingOrder;

            CurrentState.CurrentPalette = CodedColors;
            //сохранить птекущую палитру цветов

            base.OnDeactivate(close);
        }

        #endregion ViewLoadedUnloaded Events

        /// <summary>
        /// 
        /// </summary>
        public void UpdateCodedColors()
        {
            IsBusy = true;


            ThreadPool.QueueUserWorkItem(state =>
            {
                //Загрузить последний вариант палитры из кеша


                var manSelect = SelectFuncs.SelectManufacturer(SelectedManufacturer.Value);
                var colors = ColorCollectionInstance.ColorCollection.CodedColors.Select(manSelect).Where(i => i.CodeString != "-").ToList();
                var maxLen = colors.Max(i => i.CodeString.Length);
                var sortSelect = SelectFuncs.SelectSort(SelectedSortingOrder.Value, maxLen);
                
                colors = colors.OrderBy(sortSelect).ToList();

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    CodedColors.Clear();
                    colors.ForEach(CodedColors.Add);
                    IsBusy = false;
                });
            });
        }

        #region Busy Properties
        /// <summary>
        /// Показывать ли экран загрузки
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
                NotifyOfPropertyChange(() => IsNotBusy);
            }
        }

        public bool IsNotBusy
        {
            get
            {
                return !_isBusy;
            }
        }

        #endregion Busy Properties

        #region Visibility Properties

        public bool DeleteSelectedVisible
        {
            get { return _deleteSelectedVisible; }
            set
            {
                if (value.Equals(_deleteSelectedVisible)) return;
                _deleteSelectedVisible = value;
                NotifyOfPropertyChange(() => DeleteSelectedVisible);
            }
        }

        public bool ClearSelectedVisible
        {
            get { return _clearSelectedVisible; }
            set
            {
                if (value.Equals(_clearSelectedVisible)) return;
                _clearSelectedVisible = value;
                NotifyOfPropertyChange(() => ClearSelectedVisible);
            }
        }

        #endregion Visibility Properties


        #region Actions


        public void SavePalette()
        {
            var defaultName = "New palette " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            PhoneDialogs.ApproveCancel(defaultName,Save);
        }

        /// <summary>
        /// Сохранить палитру
        /// </summary>
        /// <param name="name"></param>
        private void Save(string name)
        {
            var sb = new StringBuilder();
            foreach (var displayedColor in SelectedColors)
            {
                sb.Append(displayedColor.ColorString);
                sb.Append(',');
            }

            var palette = new Palette()
            {
                ColorCodes = sb.ToString(),
                Name = name, 
                Created = DateTime.Now
            };

            try
            {
                Context.Instance.Palettes.InsertOnSubmit(palette);
                Context.Instance.SubmitChanges();

                PhoneDialogs.ShowToast("Palette saved: "+name);
            }
            catch (Exception ex)
            {
                PhoneDialogs.ShowMessage("Error saving palette " + name + ": " + ex.Message);
            }
            
        }

        public void ChangeSelectionMain(SelectionChangedEventArgs eventArgs)
        {
            ChangeSelection(eventArgs, SelectedColors);
        }

        public void ChangeSelectionSelected(SelectionChangedEventArgs eventArgs)
        {
            ChangeSelection(eventArgs, SelectedFromSelectedColors);
        }

        public void ChangeSelection<T>(SelectionChangedEventArgs eventArgs, ObservableCollection<T> collection) where T : class
        {
            foreach (var addedItem in eventArgs.AddedItems)
            {
                var item = addedItem as T;
                if (item == null) continue;
                if (!collection.Contains(item))
                {
                    collection.Insert(0, item);
                }
            }

            for (int index = eventArgs.RemovedItems.Count-1; index >= 0; index--)
            {
                var removedItem = eventArgs.RemovedItems[index];
                var item = removedItem as T;
                if (item == null) continue;
                if (collection.Contains(item)) collection.Remove(item);
            }
        }

        public void DeleteSelectedFromSelected()
        {
            for (int index = SelectedFromSelectedColors.Count-1; index >=0; index--)
            {
                var color = SelectedFromSelectedColors[index];
                SelectedColors.Remove(color);
            }
        }

        public void GetHelp()
        {
            PhoneDialogs.ShowMessage("\u2022 Swipe to List of palettes to select from existing palettes" +
                                     "\n\u2022 Select Colors from color table to compose new palette" +
                                     "\n\u2022 Press Save to save new palette in a List of palettes" +
                                     "\n\u2022 Press Clear to clear selected colors" +
                                     "\n\u2022 Press Delete to delete selected colors from new palette");
        }

        public void GotoMain()
        {
            _navigationService.UriFor<MainPageViewModel>().Navigate();
        }

        #endregion Actions



        #region Selected sorting fields

        public DescriptionObject<EnumManufacturers> SelectedManufacturer
        {
            get
            {
                return _selectedManufacturer;
            }
            set
            {
                if (Equals(value, _selectedManufacturer)) return;
                _selectedManufacturer = value;
                NotifyOfPropertyChange(() => SelectedManufacturer);
                UpdateCodedColors();
            }
        }

        public DescriptionObject<EnumSortingOrders> SelectedSortingOrder
        {
            get { return _selectedSortingOrder; }
            set
            {
                if (Equals(value, _selectedSortingOrder)) return;
                _selectedSortingOrder = value;
                NotifyOfPropertyChange(() => SelectedSortingOrder);
                UpdateCodedColors();
            }
        }

        #endregion Selected sorting fields

        #region Collections

        public ObservableCollection<DisplayedColor> CodedColors
        {
            get { return _codedColors; }
            set
            {
                _codedColors = value; 
                NotifyOfPropertyChange(() => CodedColors);
            }
        }




        public ObservableCollection<DisplayedColor> SelectedColors
        {
            get; set;
        }

        public ObservableCollection<DisplayedColor> SelectedFromSelectedColors
        {
            get; set;
        }

        public ObservableCollection<DescriptionObject<EnumManufacturers>> Manufacturers
        {
            get; set;
        }

        public ObservableCollection<DescriptionObject<EnumSortingOrders>> SortingOrders
        {
            get; set;
        }

        #endregion Collections

    }

    public class DisplayedColor
    {
        public string CodeString { get; set; }
        public string ColorString { get; set; }
    }
    
}
    