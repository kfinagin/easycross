﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Caliburn.Micro;
using EasyCross.WinPhone.Model;
using Microsoft.Win32.SafeHandles;

namespace EasyCross.WinPhone.ViewModels
{
    public class PaletteViewModel : Screen
    {
        private string _selectedManufacturer;
        private ObservableCollection<DisplayedColor> _codedColors;
        private bool _isBusy;
        private string _busyMessage;
        private string _selectedSortingOrder;

        

        public PaletteViewModel()
        {
            CodedColors = new ObservableCollection<DisplayedColor>();
            SelectedColors = new BindableCollection<DisplayedColor>();
            

            //TODO enums
            Manufacturers = new ObservableCollection<string>()
            {
                "Anchor",
                "DMC",
                "Madeira",
                "Gamma"
            };

            SortingOrders = new ObservableCollection<string>()
            {
                "Sort: color",
                "Sort: code",
            };

            _selectedManufacturer = "Anchor";
            _selectedSortingOrder = "Sort: color";

        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            UpdateCodedColors();
        }

        public void UpdateCodedColors()
        {
            IsBusy = true;

            ThreadPool.QueueUserWorkItem(state =>
            {

                var manSelect = SelectManufacturer(SelectedManufacturer);
                var colors = ColorCollectionInstance.ColorCollection.CodedColors.Select(manSelect).Where(i => i.CodeString != "-").ToList();
                var maxLen = colors.Max(i => i.CodeString.Length);

                var sortSelect = SelectSort("color", maxLen);
                
                colors = colors.OrderBy(sortSelect).ToList();

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    CodedColors.Clear();
                    colors.ForEach(CodedColors.Add);
                    IsBusy = false;

                });
            });
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
                NotifyOfPropertyChange(() => IsNotBusy);
            }
        }

        public bool IsNotBusy
        {
            get
            {
                return !_isBusy;
            }
        }

        #region Actions

        public void SavePalette()
        {
            

            //create new palette in memory and save it

        }

        public void ChangeSelection(SelectionChangedEventArgs eventArgs)
        {
            foreach (var addedItem in eventArgs.AddedItems)
            {
                var item = addedItem as DisplayedColor;
                if (item == null) continue;
                if(!SelectedColors.Contains(item)) SelectedColors.Add(item);
            }

            for (int index = eventArgs.RemovedItems.Count-1; index >= 0; index--)
            {
                var removedItem = eventArgs.RemovedItems[index];
                var item = removedItem as DisplayedColor;
                if (item == null) continue;
                if (SelectedColors.Contains(item)) SelectedColors.Remove(item);
            }
        }

        #endregion Actions

        #region Select Funcs

        public Func<DisplayedColor, string> SelectSort(string sortString, int maxLen)
        {
            
            
            switch (sortString)
            {
                case "color":
                    return i => ColorUtilities.RgbToHsv(i.ColorString);
                case "code":
                    return i => i.CodeString.Trim('*').PadLeft(maxLen,'0');
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

        public Func<CodedColor, DisplayedColor> SelectManufacturer(string manufacturer)
        {
            switch (manufacturer)
            {
                case "Anchor":
                    return i => new DisplayedColor() { CodeString = i.CodeAnchor, ColorString = i.ColorString };
                case "DMC":
                    return i => new DisplayedColor() { CodeString = i.CodeDMC, ColorString = i.ColorString };
                case "Madeira":
                    return i => new DisplayedColor() { CodeString = i.CodeMadeira, ColorString = i.ColorString };
                case "Gamma":
                    return i => new DisplayedColor() { CodeString = i.CodeGamma, ColorString = i.ColorString };
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

        #endregion Select Funcs

        #region Selected sorting fields

        public string SelectedManufacturer
        {
            get
            {
                return _selectedManufacturer;
            }
            set
            {
                if (value == _selectedManufacturer) return;
                _selectedManufacturer = value;
                NotifyOfPropertyChange(() => SelectedManufacturer);
                UpdateCodedColors();
            }
        }

        public string SelectedSortingOrder
        {
            get { return _selectedSortingOrder; }
            set
            {
                if (value == _selectedSortingOrder) return;
                _selectedSortingOrder = value;
                NotifyOfPropertyChange(() => SelectedSortingOrder);
                UpdateCodedColors();
            }
        }

        #endregion Selected sorting fields

        #region Collections

        public ObservableCollection<DisplayedColor> CodedColors
        {
            get { return _codedColors; }
            set
            {
                _codedColors = value; 
                NotifyOfPropertyChange(() => CodedColors);
            }
        }


        public ObservableCollection<DisplayedColor> SelectedColors
        {
            get; set;
        }


        public ObservableCollection<string> Manufacturers
        {
            get; set;
        }

        public ObservableCollection<string> SortingOrders
        {
            get; set;
        }

        #endregion Collections

    }

    public class DisplayedColor
    {
        public string CodeString { get; set; }
        public string ColorString { get; set; }
    }
    
}
    