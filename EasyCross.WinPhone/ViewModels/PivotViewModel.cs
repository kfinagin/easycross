﻿using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;

namespace EasyCross.WinPhone.ViewModels
{
    public class PivotViewModel: Conductor<IScreen>.Collection.OneActive
    {
        private readonly ImageViewModel _imageItem;
        private readonly SchemeViewModel _schemeItem;


        public PivotViewModel(ImageViewModel imageItem, SchemeViewModel schemeItem)
        {
            this._imageItem = imageItem;
            this._schemeItem = schemeItem;

            this.Activated += (sender, args) => AppBarConductor.Mixin(this);
        }



        protected override void OnInitialize()
        {
            base.OnInitialize();

            Items.Add(_imageItem);
            Items.Add(_schemeItem);

            ActivateItem(_imageItem);
        }
    }
}
