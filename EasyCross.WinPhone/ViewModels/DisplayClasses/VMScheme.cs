﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCross.WinPhone.ViewModels.DisplayClasses
{
    public class VMScheme
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Modified { get; set; }
    }
}
