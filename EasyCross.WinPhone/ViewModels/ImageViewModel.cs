﻿using System;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Windows.Storage;
using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;
using Coding4Fun.Toolkit.Controls.Common;
using EasyCross.WinPhone.Model;
using EasyCross.WinPhone.Model.BitmapManipulation;
using EasyCross.WinPhone.Model.Data;
using EasyCross.WinPhone.ViewModels.Helpers;
using Microsoft.Phone.Tasks;
using Microsoft.Xna.Framework;
using TaskResult = Microsoft.Phone.Tasks.TaskResult;

namespace EasyCross.WinPhone.ViewModels
{
    public class ImageViewModel: Screen
    {
        private readonly INavigationService _navigationService;
        private Scheme _currentScheme;
        private bool _previewToggled;
        private string _imagePlaceholderText;
        private BitmapImage _displayedImage;
        private int _height;
        private int _width;

        #region Constructor
        public ImageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            DisplayName = "Image";
        }

        #endregion Constructor

        protected override void OnActivate()
        {
            ThreadPool.QueueUserWorkItem(s =>
            {
                CurrentScheme = CurrentState.CurrentScheme ?? new Scheme();
                CurrentState.CurrentScheme = CurrentScheme;

                base.OnActivate();
                NotifyOfPropertyChange(() => CurrentScheme);
                NotifyOfPropertyChange(() => DisplayedImage);
                NotifyOfPropertyChange(() => Width);
                NotifyOfPropertyChange(() => Height);

            });


        }



        #region Properties

        public BitmapImage DisplayedImage
        {
            get {
                if (CurrentScheme == null) return null;

                if (PreviewToggled)
                {
                    return CurrentScheme.QuantizedImage;
                }
                else
                {
                    return CurrentScheme.InitialImage;
                }
            }
        }

        public Scheme CurrentScheme
        {
            get { return _currentScheme; }
            set
            {
                if (Equals(value, _currentScheme)) return;
                _currentScheme = value;
                NotifyOfPropertyChange(() => CurrentScheme);
            }
        }

        public bool PreviewToggled
        {
            get { return _previewToggled; }
            set
            {
                ThreadPool.QueueUserWorkItem((s) =>
                {
                    if (value.Equals(_previewToggled)) return;
                    _previewToggled = value;

                    ImagePlaceholderText = value ? "(Preview not loaded)" : "(Image not loaded)";

                    NotifyOfPropertyChange(() => PreviewToggled);
                    NotifyOfPropertyChange(() => DisplayedImage);
                });

            }
        }

        public string ImagePlaceholderText
        {
            get { return _imagePlaceholderText; }
            set
            {
                if (value == _imagePlaceholderText) return;
                _imagePlaceholderText = value;
                NotifyOfPropertyChange(() => ImagePlaceholderText);
            }
        }

        #endregion Properties

        #region Actions

        public int Height
        {
            get
            {
                if (CurrentScheme == null) return 100;
                return CurrentScheme.Height;
            }
            set
            {
                CurrentScheme.Height = value;
                CurrentScheme.Width = (int) (value*CurrentScheme.Ratio);
                NotifyOfPropertyChange(() => Height);
                NotifyOfPropertyChange(() => Width);

            }
        }

        public int Width
        {
            get
            {
                if (CurrentScheme == null) return 100;
                return CurrentScheme.Width;
            }
            set
            {
                CurrentScheme.Width = value;
                CurrentScheme.Height = (int) (value/CurrentScheme.Ratio);
                NotifyOfPropertyChange(() => Width);
                NotifyOfPropertyChange(() => Height);

            }
        }


        public void GotoMain()
        {
            _navigationService.UriFor<MainPageViewModel>().Navigate();
        }



        #region Save 

        public void SaveScheme()
        {
            if (this.CurrentScheme.Id == 0)
            {
                this.CurrentScheme.Name = "New scheme " + DateTime.Now.ToString("g");
            }

            PhoneDialogs.ApproveCancel(this.CurrentScheme.Name, Save);
        }


        private void Save(string s)
        {
            if (CurrentScheme == null) return;

            var errors = CurrentScheme.ValidationErrors();

            if (errors.Count == 0)
            {

                CurrentScheme.Edited = DateTime.Now;

                if (CurrentScheme.Id == 0)
                {
                    Context.Instance.Schemes.InsertOnSubmit(CurrentScheme);
                }

                Context.Instance.SubmitChanges();
                


                CurrentState.CurrentScheme = CurrentScheme;
                CurrentScheme.Edited = DateTime.Now;

                PhoneDialogs.ShowToast("Scheme successfully saved");

            }
            else
            {
                var sb = new StringBuilder();
                errors.ForEach(s1 => sb.AppendLine(s1));

                PhoneDialogs.ShowMessage(sb.ToString());
            }

        }

        #endregion Save

        #region Load Iimages or photos

        public void LoadImage()
        {
            var photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += (sender, result) => GetImage(result);
            photoChooserTask.Show();
            
        }

        public void TakePhoto()
        {
            var captureTask = new CameraCaptureTask();
            captureTask.Completed += (sender, result) => GetImage(result);
            captureTask.Show();
        }

        private void GetImage(PhotoResult result)
        {
            if (result.TaskResult == TaskResult.OK)
            {
                var imagePath = Path.GetFileName(result.OriginalFileName);
                CurrentScheme.ImageFileString = imagePath;
                
                NotifyOfPropertyChange(() => DisplayedImage);
            }
        }

        #endregion


        public void SelectPalette()
        {
            //перейти к выбору палитры
            _navigationService.UriFor<PalettePivotViewModel>().Navigate();
            

        }

        public void GetHelp()
        {
            PhoneDialogs.ShowMessage("\u2022 Press Load photo or Take photo to load image from gallery or camera to use it as a scheme image" +
                                     "\n\u2022 Press Palette to compose new palette for the scheme or pick one from List of palettes" +
                                     "\n\u2022 Input width and height for the desired size of your canvas" +
                                     "\n\u2022 Press Toggle Preview to view your resulting canvas with selected width, height and palette" +
                                     "\n\u2022 Select Manufacturer to view floss colors by manufacturer code" +
                                     "\n\u2022 Select Sorting Order to sort colors by hue, saturation, brightness or color code" +
                                     "\n\u2022 Swipe to Scheme to view and navigate your scheme");
        }

        #endregion Actions

    }
}
