﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Caliburn.Micro;
using EasyCross.WinPhone.Model;
using EasyCross.WinPhone.Model.Enums;
using EasyCross.WinPhone.Model.Palettes;
using EasyCross.WinPhone.ViewModels.Helpers;

namespace EasyCross.WinPhone.ViewModels
{



    public class SchemeViewModel : Screen
    {
        private readonly INavigationService _navigationService;

        public SchemeViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            DisplayName = "Scheme";
            Size = 10;
            FontSize = 30;

            Manufacturers = CurrentState.Manufacturers;
            SelectedManufacturer = Manufacturers[0];



        }

        protected override void OnActivate()
        {

            
            SetSizes();
            NotifyOfPropertyChange(() => PaletteCodes);


            if (CurrentState.CurrentScheme == null) return;
            if (CurrentState.CurrentScheme.QuantizedBitmap == null) return;

            _width = CurrentState.CurrentScheme.QuantizedBitmap.PixelWidth;
            _height = CurrentState.CurrentScheme.QuantizedBitmap.PixelHeight;
            _allPixels = CurrentState.CurrentScheme.QuantizedBitmap.Pixels;

            ThreadPool.QueueUserWorkItem(async s =>
            {
                Pixels = await GetSquares(_allPixels, _x, _y, _width, _height, Size);
                //TODO dispatcher manager
                Deployment.Current.Dispatcher.BeginInvoke(() => NotifyOfPropertyChange(() => Pixels));
            });
            

        }


        private void SetSizes()
        {

            var screenWidth = Application.Current.Host.Content.ActualWidth;
            var screenHeight = Application.Current.Host.Content.ActualHeight;

            //set rectancle size
            PixelRectancle.RectangleSize = (int) ((screenWidth-80)/(Size)-2);
        }

        #region Indexes Properties

        public int XMin
        {
            get { return _xMin; }
            set
            {
                if (value == _xMin) return;
                _xMin = value;
                NotifyOfPropertyChange(() => XMin);
            }
        }

        public int XMax
        {
            get { return _xMax; }
            set
            {
                if (value == _xMax) return;
                _xMax = value;
                NotifyOfPropertyChange(() => XMax);
            }
        }

        public int YMin
        {
            get { return _yMin; }
            set
            {
                if (value == _yMin) return;
                _yMin = value;
                NotifyOfPropertyChange(() => YMin);
            }
        }

        public int YMax
        {
            get { return _yMax; }
            set
            {
                if (value == _yMax) return;
                _yMax = value;
                NotifyOfPropertyChange(() => YMax);
            }
        }

        #endregion Indexes Properties

        /// <summary>
        /// Get subsquare of pixels
        /// </summary>
        /// <param name="pixels">Pixel array</param>
        /// <param name="x">square coordinate x (in number of squares)</param>
        /// <param name="y">square coordinate y (in number of squares)</param>
        /// <param name="w">array image width</param>
        /// <param name="h">array image height</param>
        /// <param name="size">size of square</param>
        /// <returns></returns>
        private async Task<List<List<PixelRectancle>>> GetSquares(int[] pixels, int x, int y, int w, int h, int size)
        {
            var outerList = new List<List<PixelRectancle>>();

            //TODO progress reporting - but better switch to creating image programmatically

            YMin = y;
            YMax = y + size;
            XMin = x;
            XMax = x + size;

            for (int i = 0; i < size && i<h; i++)
            {

                var innerList = new List<PixelRectancle>();

                for (int j = 0; j < size && j<w; j++)
                {
                    
                    int pixelCoordinate = (y + i)*w + x + j;

                    
                    int value;

                    if (x + j > w || y+i>h || pixelCoordinate >= w*h)
                        value = 0;
                    else
                        value = pixels[pixelCoordinate];


                    byte[] colorArray = BitConverter.GetBytes(value);
                    byte b = colorArray[0];
                    byte g = colorArray[1];
                    byte r = colorArray[2];

                    var colorString = "#" + r.ToString("X2") + g.ToString("X2") + b.ToString("X2");

                    innerList.Add(new PixelRectancle(){ColorString = colorString, Code = CurrentState.CurrentScheme.Palette.GetCode(colorString)});


                }
                outerList.Add(innerList);

            }
            return outerList;
        }


        #region Private
        //кооринаты текущего квадрата
        private int _x = 0;
        private int _y = 0;

        private int _size;
        private List<List<PixelRectancle>> _pixels;
        private int _width;
        private int _height;
        private int[] _allPixels;
        private int _xMin;
        private int _xMax;
        private int _yMin;
        private int _yMax;
        private string _zoomText;
        private int _fontSize;
        private DescriptionObject<EnumManufacturers> _selectedManufacturer;


        #endregion Private

        #region Pixel and Size Properties

        public List<List<PixelRectancle>> Pixels
        {
            get { return _pixels; }
            set
            {
                if (Equals(value, _pixels)) return;
                _pixels = value;
                NotifyOfPropertyChange(() => Pixels);
            }
        }

        public int Size
        {
            get { return _size; }
            set
            {
                if (value == _size) return;
                _size = value;
                NotifyOfPropertyChange(() => Size);
                ZoomText = "Zoom: " + Size + " squares";
            }
        }

        public string ZoomText
        {
            get { return _zoomText; }
            set
            {
                if (value == _zoomText) return;
                _zoomText = value;
                NotifyOfPropertyChange(() => ZoomText);
            }
        }

        public int FontSize
        {
            get { return _fontSize; }
            set
            {
                if (value == _fontSize) return;
                _fontSize = value;
                NotifyOfPropertyChange(() => FontSize);
            }
        }

        #endregion Pixel and Size Properties

        #region Actions
        public void GotoMain()
        {
            _navigationService.UriFor<MainPageViewModel>().Navigate();
        }
        public async void ZoomMode()
        {
            do
            {
                if (Size == 10)
                {
                    Size = 20;
                    FontSize = 14;
                    break;
                }
                if (Size == 20)
                {
                    Size = 10;
                    FontSize = 30;
                    break;
                }
            } while (false);

            SetSizes();
            Pixels = await GetSquares(_allPixels, 0, 0, _width, _height, Size);
        }


        public async void Left()
        {
            _x-=Size/2;
            if (_x < 0) _x = 0;
            Pixels = await GetSquares(_allPixels,_x,_y,_width,_height,Size);

        }

        public async void Right()
        {
            _x+=Size/2;
            if (_x+Size > _width) _x = _width-Size;
            Pixels = await GetSquares(_allPixels, _x, _y, _width, _height, Size);
        }

        public async void Up()
        {
            _y-=Size/2;
            if (_y < 0) _y = 0;
            Pixels = await GetSquares(_allPixels, _x, _y, _width, _height, Size);
        }

        public async void Down()
        {
            _y+=Size/2;
            if (_y+Size > _height) _y = _height-Size;
            Pixels = await GetSquares(_allPixels, _x, _y, _width, _height, Size);
        }

        #endregion Actions

        #region Palette Legend

        //Palette PaletteCodes
        public ObservableCollection<int> PaletteCodes
        {
            get
            {
                if (CurrentState.CurrentScheme == null) return null;
                if (CurrentState.CurrentScheme.Palette == null) return null;


                var a = new ObservableCollection<int>();
                for (int i = 0; i < CurrentState.CurrentScheme.Palette.Colors.Count; i++)
                {
                    a.Add(i);
                }
                return a;
            }
        }


        private int? _selectedPaletteCode;

        //Selected Palette Code
        public int? SelectedPaletteCode
        {
            get
            {
                
                return _selectedPaletteCode;
            }
            set
            {
                if (value == _selectedPaletteCode) return;
                _selectedPaletteCode = value;
                NotifyOfPropertyChange(() => SelectedPaletteCode);
                NotifyOfPropertyChange(() => CurrentColor);
            }
        }

        //Manufacturers
        public ObservableCollection<DescriptionObject<EnumManufacturers>> Manufacturers
        {
            get;
            set;
        }

        //Selected Manufacturer
        public DescriptionObject<EnumManufacturers> SelectedManufacturer
        {
            get
            {
                return _selectedManufacturer;
            }
            set
            {
                if (Equals(value, _selectedManufacturer)) return;
                _selectedManufacturer = value;
                NotifyOfPropertyChange(() => SelectedManufacturer);
                NotifyOfPropertyChange(() => CurrentColor);
            }
        }

        //Current Color Code
        public DisplayedColor CurrentColor
        {
            get
            {
                if (CurrentState.CurrentScheme == null) return null;
                if (CurrentState.CurrentScheme.Palette == null) return null;

                var curMan = SelectFuncs.SelectManufacturer(SelectedManufacturer.Value);
                var curIndex = SelectedPaletteCode;

                if (SelectedPaletteCode != null)
                {
                    var colorString =
                        CurrentState.CurrentScheme.Palette.Colors.Select(curMan).ToList()[SelectedPaletteCode.Value];
                    return colorString;
                }
                else
                {
                    return null;
                }
            }
        }

        public void LoadColor(PixelRectancle p)
        {
            SelectedPaletteCode = p.Code;

        }

        public void GoToCoordinate()
        {
            PhoneDialogs.SetXY(async delegate(int x, int y)
            {
                if (x < 0 || x > _width - Size) return;
                if (y < 0 || y > _height - Size) return;

                _x = x;
                _y = y;

                Pixels = await GetSquares(_allPixels, _x, _y, _width, _height, Size);
                NotifyOfPropertyChange(() => Pixels);
            });
        }

        #endregion Palette Legend



    }
}
