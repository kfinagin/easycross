﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Caliburn.Micro;
using Coding4Fun.Toolkit.Controls;
using EasyCross.WinPhone.Model;
using EasyCross.WinPhone.Model.Data;
using EasyCross.WinPhone.ViewModels.Helpers;
using Microsoft.Phone.Reactive;

namespace EasyCross.WinPhone.ViewModels
{


    public class MainPageViewModel : Screen
    {
        private readonly INavigationService _navigationService;

        public MainPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            AvaliableSchemes = new ObservableCollection<Scheme>();

            SelectedSchemes = new ObservableCollection<Scheme>();
            SelectedSchemes.CollectionChanged += SelectedSchemes_CollectionChanged;

        }

        void SelectedSchemes_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var number = SelectedSchemes.Count;

            if (number <= 0)
            {
                EditSchemeVisible = false;
                DeleteSchemeVisible = false;
            }

            else 
            {
                EditSchemeVisible = true;
                DeleteSchemeVisible = true;
            }

        }

        public void ChangeSelection(SelectionChangedEventArgs eventArgs)
        {
            foreach (var addedItem in eventArgs.AddedItems)
            {
                var item = addedItem as Scheme;
                if (item == null) continue;
                if (!SelectedSchemes.Contains(item))
                {
                    SelectedSchemes.Insert(0,item);
                }
            }

            foreach (var removedItem in eventArgs.RemovedItems)
            {
                var item = removedItem as Scheme;
                if (item == null) continue;
                if (SelectedSchemes.Contains(item))
                {
                    SelectedSchemes.Remove(item);
                }
            }
            if(SelectedSchemes.Count>0) CurrentState.CurrentScheme = SelectedSchemes[0];

        }

        protected override void OnActivate()
        {
            //reload schemes
            base.OnActivate();
            ReloadSchemes();

        }

        #region Properties


        private Scheme _selectedScheme;
        private bool _addSchemeVisible;
        private bool _editSchemeVisible;
        private bool _deleteSchemeVisible;

        public Scheme SelectedScheme
        {
            get { return _selectedScheme; }
            set
            {
                _selectedScheme = value;
                NotifyOfPropertyChange(() => SelectedScheme);
            }
        }

        #region Collections

        public ObservableCollection<Scheme> AvaliableSchemes
        {
            get; set;
        }

        public ObservableCollection<Scheme> SelectedSchemes
        {
            get; set;
        }

        #endregion Collections

        #region Visibility Properties

        public bool AddSchemeVisible
        {
            get { return true; }
            set
            {
                if (value.Equals(_addSchemeVisible)) return;
                _addSchemeVisible = value;
                NotifyOfPropertyChange(() => AddSchemeVisible);
            }
        }

        public bool EditSchemeVisible
        {
            get { return _editSchemeVisible; }
            set
            {
                if (value.Equals(_editSchemeVisible)) return;
                _editSchemeVisible = value;
                NotifyOfPropertyChange(() => EditSchemeVisible);
            }
        }

        public bool DeleteSchemeVisible
        {
            get { return _deleteSchemeVisible; }
            set
            {
                if (value.Equals(_deleteSchemeVisible)) return;
                _deleteSchemeVisible = value;
                NotifyOfPropertyChange(() => DeleteSchemeVisible);
            }
        }

        #endregion Visibility Properties

        #endregion Properties

        #region Actions

        public void ReloadSchemes()
        {
            //загрузка схем из хранилища
            AvaliableSchemes.Clear();
            SelectedSchemes.Clear();

            foreach (var scheme in Context.Instance.Schemes.OrderByDescending(i => i.Edited))
            {
                AvaliableSchemes.Add(scheme);    
            }

        }

        public void AddScheme()
        {
            CurrentState.CurrentScheme = null;
            _navigationService.UriFor<PivotViewModel>().Navigate();
        }

        public void EditScheme()
        {
            //TODO Existing Current scheme
            _navigationService.UriFor<PivotViewModel>().Navigate();
        }

        public void DeleteScheme()
        {
            PhoneDialogs.DeleteCancel(Delete);
        }

        private void Delete()
        {
            try
            {
                Context.Instance.Schemes.DeleteOnSubmit(SelectedSchemes[0]);
                Context.Instance.SubmitChanges();
                ReloadSchemes();

                PhoneDialogs.ShowMessage("Deleted");
            }
            catch (Exception ex)
            {
                PhoneDialogs.ShowMessage("Error: "+ ex.Message);
            }

        }

        public void ViewColors()
        {
            _navigationService.UriFor<PalettePivotViewModel>().Navigate();
        }

        #endregion Actions







    }
}
