﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;

namespace EasyCross.WinPhone.ViewModels
{
    public class PalettePivotViewModel : Conductor<IScreen>.Collection.OneActive
    {
        private readonly PaletteViewModel _paletteItem;
        private readonly PaletteListViewModel _paletteListItem;

        public PalettePivotViewModel(PaletteViewModel paletteItem, PaletteListViewModel paletteListItem)
        {
            _paletteItem = paletteItem;
            _paletteListItem = paletteListItem;

            this.Activated += (sender, args) => AppBarConductor.Mixin(this);
        }

      

        protected override void OnInitialize()
        {
            base.OnInitialize();

            Items.Add(_paletteItem);
            Items.Add(_paletteListItem);
        }
    }
}
