﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCross.ImageProcessing.Helpers
{
    public class Array2D<T> : IEnumerable<T>
    {

        private List<T> _data;

        public int Width { get; private set; }
        public int Height { get; private set; }

        #region Constructors

        public Array2D(int width, int height, T defaultValue = default(T))
        {
            Width = width;
            Height = height;

            _data = new List<T>();
            _data = Enumerable.Repeat(defaultValue, Width*Height).ToList();
        }

        public Array2D(Array2D<T> array2D)
        {
            this.Width = array2D.Width;
            this.Height = array2D.Height;

            _data = new List<T>();
            _data.AddRange(array2D);
        }

        #endregion Constructors

        #region Math operations

        public static Array2D<T> operator *(Array2D<T> array, T scalar)
        {
            throw new NotImplementedException();
            //TODO
        }

        public static List<T> operator *(Array2D<T> array, List<T> vector)
        {
            throw new NotImplementedException();
        }


        public static Array2D<T> MultiplyRowScalar(Array2D<T> array, int row, double mult)
        {
            throw new NotImplementedException();
        }

        public static Array2D<T> AddRowMultiple(int fromRow, int toRow, double mult)
        {
            throw new NotImplementedException();    
        }

        public static Array2D<T> MatrixInverse()
        {
            throw new NotImplementedException();
        }


        #endregion Math operations



        public T this[int col,int row]
        {
            get { return _data[row*Width+col]; }
            set
            {
                _data[row*Width + col] = value;
            }
        }

        #region IEnumerable implementation

        public IEnumerator<T> GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion Ieumerable implementation
    }
}
