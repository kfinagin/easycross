﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCross.ImageProcessing.Helpers
{
    public interface ICalculator<T>
    {
        T Add(T a,T b);
        T Multiply(T a, T b);
        T Negate(T a);
        T Subtract(T a, T b);
        T Divide(T numerator, T denominator);
    }
}
