﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCross.ImageProcessing.Helpers.Calculators
{
    public class VectorFixedCalculator<T,C> : ICalculator<VectorFixed<T,C>> where C : ICalculator<T>, new()
    {
        public VectorFixed<T, C> Add(VectorFixed<T, C> a, VectorFixed<T, C> b)
        {
            return a + b;
        }

        public VectorFixed<T, C> Multiply(VectorFixed<T, C> a, VectorFixed<T, C> b)
        {
            return a * b;
        }

        public VectorFixed<T, C> Negate(VectorFixed<T, C> a)
        {
            return - a;
        }

        public VectorFixed<T, C> Subtract(VectorFixed<T, C> a, VectorFixed<T, C> b)
        {
            return a - b;
        }

        public VectorFixed<T, C> Divide(VectorFixed<T, C> numerator, VectorFixed<T, C> denominator)
        {
            throw new NotImplementedException("Divide operation for vector calculators is not implemented");
        }
    }
}
