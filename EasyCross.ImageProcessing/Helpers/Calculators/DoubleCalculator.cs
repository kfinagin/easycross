﻿namespace EasyCross.ImageProcessing.Helpers
{
    public class DoubleCalculator : ICalculator<double>
    {
        public double Add(double a, double b)
        {
            return a + b;
        }

        public double Multiply(double a, double b)
        {
            return a * b;
        }

        public double Negate(double a)
        {
            return -a;
        }

        public double Subtract(double a, double b)
        {
            return a - b;
        }

        public double Divide(double numerator, double denominator)
        {
            return numerator / denominator;
        }
    }
}