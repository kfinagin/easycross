namespace EasyCross.ImageProcessing.Helpers
{
    public class IntCalculator : ICalculator<int>
    {
        public int Add(int a, int b)
        {
            return a + b;
        }

        public int Multiply(int a, int b)
        {
            return a * b;
        }

        public int Negate(int a)
        {
            return -a;
        }

        public int Subtract(int a, int b)
        {
            return a - b;
        }

        public int Divide(int numerator, int denominator)
        {
            return numerator / denominator;
        }
    }
}