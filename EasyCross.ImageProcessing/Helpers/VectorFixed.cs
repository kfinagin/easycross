﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyCross.ImageProcessing.Helpers
{
    /// <summary>
    /// Class for fixed vector of double triplets, used commonly throughout the algorithm
    /// </summary>
    public class VectorFixedD3 : VectorFixed<double, DoubleCalculator>
    {
        public VectorFixedD3() : base()
        {
            
        }

        public VectorFixedD3(VectorFixed<double, DoubleCalculator> vector) : base(3,0.0)
        {
            for (int i = 0; i < 3; i++)
            {
                _data[i] = vector[i];
            }
        }

        public static VectorFixedD3 operator +(VectorFixedD3 v1, VectorFixed<double, DoubleCalculator> v2)
        {
            return new VectorFixedD3(v1 as VectorFixed<double,DoubleCalculator> + v2);
        }

        public static VectorFixedD3 operator *(VectorFixedD3 v1, VectorFixed<double, DoubleCalculator> v2)
        {
            return new VectorFixedD3((v1 as VectorFixed<double,DoubleCalculator>) * v2);
        }

    }

    /// <summary>
    /// Generic class for fixed vector, supporting math vector operations
    /// </summary>
    /// <typeparam name="T">Type for vector to contain</typeparam>
    /// <typeparam name="C">Calculator class, that support operations performed with T type</typeparam>
    public class VectorFixed<T,C> : IEnumerable where C: ICalculator<T>, new()
    {
        private static readonly C Calc = new C();

        protected readonly List<T> _data;

        public int Length{get { return _data.Count; }}

        #region Constructors

        public VectorFixed()
        {
            _data = new List<T>();
        }

        public VectorFixed(int length, T defaultValue = default(T))
        {
            _data = new List<T>();
            _data = Enumerable.Repeat(defaultValue, length).ToList();
        }

        public VectorFixed(VectorFixed<T, C> vectorFixed) : this(vectorFixed.Length)
        {
            for (int i = 0; i < vectorFixed.Length; i++)
            {
                _data[i] = vectorFixed[i];
            }
        }

        public VectorFixed(List<T> vector) : this(vector.Count)
        {
            for (int i = 0; i < vector.Count; i++)
            {
                _data[i] = vector[i];
            }
        }

        #endregion Constructors

        #region Math Functions

        T NormSquared()
        {
            return VectorToScalarMappingTwoOperations(this, this, Calc.Multiply, Calc.Add);
        }

        public VectorFixed<T, C> DirectProduct(VectorFixed<T, C> rhs)
        {
            return VectorBinaryOperation(this, rhs, Calc.Multiply);
        }

        public T DotProduct(VectorFixed<T, C> rhs)
        {
            return VectorToScalarMappingTwoOperations(this, rhs, Calc.Multiply, Calc.Add);
        }

        public static VectorFixed<T, C> operator +(VectorFixed<T, C> v1, VectorFixed<T, C> v2)
        {
            return VectorBinaryOperation(v1, v2, Calc.Add);
        }

        public static VectorFixed<T, C> operator -(VectorFixed<T, C> v1, VectorFixed<T, C> v2)
        {
            return VectorBinaryOperation(v1, v2, Calc.Subtract);
        }

        public static VectorFixed<T, C> operator +(VectorFixed<T, C> v, T s)
        {
            return VectorScalarOperation(v, s, Calc.Add);
        }

        public static VectorFixed<T, C> operator +(T s, VectorFixed<T, C> v)
        {
            return v + s;
        }

        public static  VectorFixed<T,C>  operator -(VectorFixed<T,C> v )
        {
            return VectorUnaryOperation(v, Calc.Negate);
        }

        public static VectorFixed<T, C> operator *(VectorFixed<T, C> v1, VectorFixed<T, C> v2)
        {
            return VectorBinaryOperation(v1, v2, Calc.Multiply);
        }

        public static VectorFixed<T, C> operator *(VectorFixed<T, C> v, T s)
        {
            return VectorScalarOperation(v, s, Calc.Multiply);
        }

        public static VectorFixed<T, C> operator *(T s, VectorFixed<T, C> v)
        {
            return v * s;
        }


        #endregion Math Functions

        #region Common operations

        public static VectorFixed<T, C> VectorUnaryOperation(
            VectorFixed<T,C> vectorFixed, 
            Func<T, T> unaryOperation)
        {
            var result = new VectorFixed<T, C>(vectorFixed.Length);

            for (int i = 0; i < vectorFixed.Length; i++)
            {
                result[i] = unaryOperation(vectorFixed[i]);
            }
            return result;
        }

        public static VectorFixed<T, C> VectorBinaryOperation(
            VectorFixed<T, C> vector1, 
            VectorFixed<T, C> vector2,
            Func<T, T, T> binaryOperation)
        {
            if(vector1.Length!=vector2.Length) throw new ArithmeticException("Vector lengths are not equal");
            var result = new VectorFixed<T, C>(vector1.Length);

            for (int i = 0; i < vector1.Length; i++)
            {
                result[i] = binaryOperation(vector1[i], vector2[i]);
            }
            return result;
        }

        public static VectorFixed<T, C> VectorScalarOperation(
            VectorFixed<T, C> vector,
            T scalar,
            Func<T, T, T> binaryOperation
            )
        {
            var result = new VectorFixed<T, C>(vector.Length);

            for (int i = 0; i < vector.Length; i++)
            {
                result[i] = binaryOperation(vector[i], scalar);
            }

            return result;
        }

        public static T VectorToScalarMappingTwoOperations(
            VectorFixed<T, C> vector1,
            VectorFixed<T, C> vector2,
            Func<T, T, T> inner,
            Func<T, T, T> outer
            )
        {
            T result = default(T);

            if(vector1.Length!=vector2.Length) throw new ArithmeticException("Vector lengths are not equal");

            for (int i = 0; i < vector1.Length; i++)
            {
                result = outer(result, inner(vector1[i], vector2[i]));
            }

            return result;
        }

        #endregion Common operations

        //Indexer
        public T this[int i]
        {
            get { return _data[i]; }
            set
            {
                _data[i] = value;
            }
        }


        public void Add(T i)
        {
            _data.Add(i);
        }


        public IEnumerator GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            foreach (var v in _data)
            {
                sb.Append(v);
                sb.Append(" ");
            }

            return sb.ToString();
        }
    }
}
