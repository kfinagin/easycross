﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyCross.ImageProcessing.Helpers;
using EasyCross.ImageProcessing.Helpers.Calculators;

namespace EasyCross.ImageProcessing.Quantization.Quantizers.SColorQ
{
    public class SpatialColorQuantizer : BaseQuantizer
    {

        public SpatialColorQuantizer()
        {
            
        }



        //image to quantize

        //quantized image



        //filter_weights

        //palette

        //other settings?

        //initial temperature
        //final temperature
        //temps_per_level
        //repeats per level 

        private Array3D<double> _coarseVariables; 

        private Array2D<VectorFixedD3> _filterWeights;

        private Array2D<VectorFixedD3> _image;

        private List<VectorFixedD3> _palette;


        private double _initialTemperature;
        private double _finalTemperature;

        private int _tempsPerLevel;
 

        public void Quantize()
        {
            //compute max coarse level
            //TODO? strategy
            int maxCoarseLevel = ComputeMaxCoarseLevel(_image.Width, _image.Height);

            var coarseVariables = new Array3D<double>(
                _image.Width >> maxCoarseLevel,
                _image.Height >> maxCoarseLevel,
                _palette.Count);

            coarseVariables.FillRandom();



            int extendedNeighborhoodWidth = _filterWeights.Width*2 - 1;
            int extendedNeighborhoodHeight = _filterWeights.Height*2 - 1;

            var b0 = new Array2D<VectorFixedD3>(extendedNeighborhoodWidth, extendedNeighborhoodHeight);

            //compute b array
            ComputeBArray(_filterWeights, b0);

            var a0 = new Array2D<VectorFixedD3>(_image.Width, _image.Height);

            ComputeAImage(_image, b0, a0);


            var aVec = new List<Array2D<VectorFixedD3>>();
            var bVec = new List<Array2D<VectorFixedD3>>();

            aVec.Add(a0);
            bVec.Add(b0);

            int coarseLevel;
            for (coarseLevel = 1; coarseLevel <= maxCoarseLevel; coarseLevel++)
            {
                int radiusWidth = (_filterWeights.Width - 1)/2;
                int radiusHeight = (_filterWeights.Height - 1)/2;

                int bWidth = Math.Max(3, bVec[bVec.Count - 1].Width - 2);
                int bHeight = Math.Max(3, bVec[bVec.Count - 1].Height - 2);

                var bi = new Array2D<VectorFixedD3>(bWidth, bHeight);


		        for(int J_y=0; J_y<bi.Height; J_y++) {
			        for(int J_x=0; J_x<bi.Width; J_x++) {

				        for(int i_y=radiusHeight*2; i_y<radiusHeight*2+2; i_y++) {
					        for(int i_x=radiusWidth*2; i_x<radiusWidth*2+2; i_x++) {

						        for(int j_y=J_y*2; j_y<J_y*2+2; j_y++) {
							        for(int j_x=J_x*2; j_x<J_x*2+2; j_x++) {
								        bi[J_x,J_y] += BValue(bVec[bVec.Count-1], i_x, i_y, j_x, j_y);
							        }
						        }
					        }
				        }
			        }
		        }
            }




            //-----------------------------------------------
            //Multiscale annealing

            coarseLevel = maxCoarseLevel;
            int itersPerLevel = _tempsPerLevel;
            double temperatureMultiplier = Math.Pow(_finalTemperature/_initialTemperature,1.0 / Math.Max(3, maxCoarseLevel*itersPerLevel));

            int iters_at_current_level = 0;
            bool skip_palette_maintenance = false;

            var s = new Array2D<VectorFixedD3>(_palette.Count, _palette.Count);

//            ComputeInitialS(s, _coarseVariables, bVec[coarseLevel]);









        }

        /// <summary>
        /// Making the coarsest layer to have at most maxPixels pixels
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private int ComputeMaxCoarseLevel(int width, int height)
        {
            const int maxPixels = 4000;
            int result = 0;
            while (width * height > maxPixels)
            {
                width >>= 1;
                height >>= 1;
                result++;
            }
            return result;
        }

        private void ComputeAImage(object image, Array2D<VectorFixedD3> b0, Array2D<VectorFixedD3> a0)
        {
            

        }

        private void ComputeBArray(object filterWeights, Array2D<VectorFixedD3> b0)
        {
            

        }


        private VectorFixedD3 BValue(Array2D<VectorFixedD3> b, int iX, int iY, int jX, int jY)
        {
            int radiusWidth = (b.Width - 1)/2;
		    int radiusHeight = (b.Height - 1)/2;

            int kX = jX - iX + radiusWidth;
	        int kY = jY - iY + radiusHeight;
            if (kX >= 0 && kY >= 0 && kX < b.Width && kY < b.Height)
                return b[kX, kY];
            else
                return new VectorFixedD3();

        }




    }
}
