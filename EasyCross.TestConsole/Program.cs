﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using EasyCross.ImageProcessing.Helpers;
using HtmlAgilityPack;

namespace EasyCross.TestConsole
{

    [Serializable]
    public class CodedColor
    {
        public string CodeDMC { get; set; }
        public string CodeGamma { get; set; }
        public string CodeAnchor { get; set; }
        public string CodeMadeira { get; set; }

        public string ColorString { get; set; }
    }

    [Serializable]
    public class ColorCollection
    {
        public ColorCollection()
        {
            CodedColors = new List<CodedColor>();
        }
        public List<CodedColor> CodedColors { get; set; } 
    }




    class Program
    {
        static void Main(string[] args)
        {
            //parse html 
            HtmlDocument doc = new HtmlWeb().Load(@"http://www.firma-gamma.ru/catalog/threads/colormap/muline/colors/");

            var colorCollection = new ColorCollection();


            var node = doc.DocumentNode.SelectNodes("//table[@class='ctab']").FirstOrDefault();

            var childNodes = node.ChildNodes;

            for (int index = 1; index < childNodes.Count; index++)
            {
                var childNode = childNodes[index];

                
                var dmcCode = childNode.ChildNodes[0].ChildNodes[0].InnerText;
                var gammaCode = childNode.ChildNodes[1].ChildNodes[0].InnerText;
                var anchorCode = childNode.ChildNodes[2].ChildNodes[0].InnerText;
                var madeiraCode = childNode.ChildNodes[3].ChildNodes[0].InnerText;

                var colorCode = childNode.ChildNodes[4].Attributes["style"].Value.Split(':')[2];

                var codedColor = new CodedColor()
                {
                    ColorString = colorCode
                };
                codedColor.CodeAnchor = TrimValue(anchorCode);
                codedColor.CodeDMC=TrimValue(dmcCode);
                codedColor.CodeGamma=TrimValue(gammaCode);
                codedColor.CodeMadeira=TrimValue(madeiraCode);

                colorCollection.CodedColors.Add(codedColor);

            }

            SerializeObject(colorCollection,"palette.xml");
            
        }

        private static string TrimValue(string codeString)
        {
            var code = codeString.Split(',')[0];

            return code.Trim();
        }

        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public static void SerializeObject<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null) { return; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(fileName);
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }
        }


        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                string attributeXml = string.Empty;

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception ex)
            {
                //Log exception here
            }

            return objectOut;
        }
    }
}
